# Generated by Django 2.1.3 on 2018-12-07 21:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_whstack_models", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="whwebsites",
            name="cluster_id",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_whstack_models.WhClusters",
            ),
            preserve_default=False,
        ),
    ]
