from datetime import datetime

from django.db import models
from publicsuffix2 import get_public_suffix

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammadAbstract,
)


class WhTenantSettings(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    dns_enabled = models.BooleanField(default=False)
    domains_enabled = models.BooleanField(default=False)

    class Meta:
        db_table = "wh_tenant_settings"


class WhClusters(models.Model):
    cluster_name = models.CharField(max_length=50)
    cluster_created = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "wh_clusters"


class WhRegistryDomains(models.Model):
    DOMAIN_ACTIVE = 0
    DOMAIN_IN_TRANSFER = 1
    DOMAIN_STATUS_UNKNOWN = 100

    DOMAIN_STATUS_LIST = (
        (DOMAIN_ACTIVE, "Active"),
        (DOMAIN_IN_TRANSFER, "In Transfer"),
        (DOMAIN_STATUS_UNKNOWN, "Status Unknown"),
    )

    id = models.BigAutoField(primary_key=True)
    domain = models.CharField(max_length=1024, unique=True)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    status = models.IntegerField(choices=DOMAIN_STATUS_LIST, null=False, blank=False)
    domain_created = models.DateTimeField(null=False, default=datetime.now)

    @property
    def get_tld(self):
        return WhIanaTld.objects.get(tld=self.domain.split(".")[-1]).tld

    class Meta:
        db_table = "wh_reg_domains"


class WhRegistryDomainTickets(SnotifierEmailContactZammadAbstract):
    domain_reg = models.ForeignKey(WhRegistryDomains, models.DO_NOTHING)

    class Meta:
        db_table = "wh_reg_domain_tickets"


class WhDomains(models.Model):
    NONE = "NONE"
    WWW = "WWW"
    WILDCARD = "WILDCARD"

    INCLUDE_SUB_CHOICES = (
        (NONE, "None"),
        (WWW, "WWW"),
        (WILDCARD, "Wildcard"),
    )

    id = models.BigAutoField(primary_key=True)
    domain = models.CharField(max_length=1024, unique=True)
    include_sub = models.CharField(
        max_length=8, choices=INCLUDE_SUB_CHOICES, default=WWW
    )
    website = models.ForeignKey("WhWebsites", models.CASCADE)
    dom_lnk_created = models.DateTimeField(null=False, default=datetime.now)

    @property
    def get_tld(self):
        return WhIanaTld.objects.get(tld=self.domain.split(".")[-1]).tld

    @property
    def get_public_suffix(self):
        # TODO: fetch updated list
        # psl_file = fetch()
        # return get_public_suffix(self.domain, psl_file)
        return get_public_suffix(self.domain)

    class Meta:
        db_table = "wh_domains"


class WhWebsiteVars(models.Model):
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)
    website = models.ForeignKey("WhWebsites", models.CASCADE)

    class Meta:
        db_table = "wh_website_vars"
        unique_together = (("website", "name"),)


class WhWebsites(models.Model):
    id = models.BigAutoField(primary_key=True)
    web_name = models.CharField(max_length=1024)
    cluster = models.ForeignKey("WhClusters", models.CASCADE)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    website_created = models.DateTimeField(null=False, default=datetime.now)

    class Meta:
        db_table = "wh_websites"


class WhIanaTld(models.Model):
    id = models.BigAutoField(primary_key=True)
    tld = models.CharField(max_length=1024)
    item_code = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "wh_iana_tld"
