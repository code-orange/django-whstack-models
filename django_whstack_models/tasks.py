from celery import shared_task

from django_whstack_models.django_whstack_models.models import *


@shared_task(name="whstack_iana_tld_import")
def whstack_iana_tld_import():
    iana_tld_file = open("data/tlds-alpha-by-domain.txt", "rt")

    for line in iana_tld_file:
        line = line.strip()
        if not line or line.startswith("//") or line.startswith("#"):
            continue

        try:
            tld = WhIanaTld.objects.get(tld=line.lower())
        except WhIanaTld.DoesNotExist:
            tld = WhIanaTld(tld=line.lower())
            tld.save(force_insert=True)

    return
